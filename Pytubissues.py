from pytube import YouTube, Playlist
import re

titles = []
i = 0
plLen = 0
plLenArray = []
maxFromArray = 0

for j in range(0,5):
    playlist = Playlist("https://www.youtube.com/playlist?list=PLNTm9yU0zou7kKcN7091Rdr322Qge5LNA")
    plLenArray.append(len(playlist))
    maxFromArray = max(plLenArray)

while plLen != maxFromArray:
    playlist = Playlist("https://www.youtube.com/playlist?list=PLNTm9yU0zou7kKcN7091Rdr322Qge5LNA")
    plLen = len(playlist)
    print("Playlist length :"+str(plLen))

links=["https://www.youtube.com/watch?v=_nNbhfPsXJg","https://www.youtube.com/watch?v=E_SKEhn5Ows","https://www.youtube.com/watch?v=2vin5A7m51c","https://www.youtube.com/watch?v=VaVixdS3bFo","https://www.youtube.com/watch?v=h0vAHZLmGDs","https://www.youtube.com/watch?v=ECdc-pr8y0w","https://www.youtube.com/watch?v=teQb8BTMA7U","https://www.youtube.com/watch?v=dWS1lUsh9rw","https://www.youtube.com/watch?v=t1KNvnWrGK8","https://www.youtube.com/watch?v=ZgPJqCJWxaA","https://www.youtube.com/watch?v=dWQMwyO2SWs","https://www.youtube.com/watch?v=IpI7eFdiM3M","https://www.youtube.com/watch?v=X6mUh7jO68U"]

playlist = list(playlist)
for link in links:
    playlist.remove(link)

print("Playlist length after removing:"+str(len(playlist)))

file1 = open("issues.csv","a+")
file1.write("title,description\n")
file1.close()

file2 = open("quickactions.csv","a+")
file2.write("title,description\n")
file2.close()

print("===================================")
for video in playlist:
    file1 = open("issues.csv","a+")
    file2 = open("quickactions.csv","a+")
    vidTitle = YouTube(video).title
    while vidTitle == "YouTube":
        vidTitle = YouTube(video).title
    titles.append(vidTitle)
    
    vidLen = str(int(YouTube(video).length*2/60))+"m"

    vidDesc = YouTube(video).description
    #Remove Project File
    vidDesc2 = re.sub(r'Project Files : *.{58}', '', vidDesc)
    #Remove Support me
    vidDesc3 = re.sub(r'Support my work on Patreon : *.{34}', '', vidDesc2)

    print('N°'+str(i)+' - '+'title : '+titles[i])
    file1.write(titles[i]+',"\n'+vidDesc3+'\n\n'+playlist[i]+'"\n\n')
    file2.write(titles[i]+',\n\n/label ~ \n/milestone % \n/estimate '+vidLen+' \n\n')
    file1.close()
    file2.close()
    i=i+1

exit()
